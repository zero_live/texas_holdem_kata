require_relative '../hand'

describe 'Texas' do
  it 'calculates points for the highest card' do
    cards = ['2c', '3h', '5d', '6c', 'Kh']
    hand = Hand.new(cards)

    points = hand.calculate_points

    expect(points).to eq(13)
  end

  it 'calculates points for a pair' do
    cards = ['2d', '2c', '3h', '6h', 'Qs']
    hand = Hand.new(cards)

    points = hand.calculate_points

    expect(points).to eq(102)
  end

  it 'calculates points for the highest pair' do
    cards = ['3h', '6h', '6s', '2d', '2c']
    hand = Hand.new(cards)

    points = hand.calculate_points

    expect(points).to eq(106)
  end
end
