
class Hand
  BASE_POINTS_FOR_PAIR = 100

  def initialize(cards)
    @cards = cards
  end

  def calculate_points
    points = 0
    points = pair if pair > points
    points = highest_card if highest_card > points
    points
  end

  private

  def pair
    points = 0

    @cards.each do |card|
      my_card = Card.new(card)

      if has_pair?(my_card.value)
        game_points = BASE_POINTS_FOR_PAIR + my_card.value
        is_best_pair = (game_points > points)

        points = game_points if is_best_pair
      end
    end

    points
  end

  def has_pair?(value)
    values.count(value) == 2
  end

  def values
    @cards.map do |card|
      Card.new(card).value
    end
  end

  def highest_card
    highest = 0

    @cards.each do |card|
      card_value = Card.new(card).value
      is_the_highest = (card_value > highest)

      highest = card_value if is_the_highest
    end

    highest
  end
end

class Card
  POINTS = {
    '2' => 2,
    '3' => 3,
    '4' => 4,
    '5' => 5,
    '6' => 6,
    '7' => 7,
    '8' => 8,
    '9' => 9,
    '10' => 10,
    'J' => 11,
    'Q' => 12,
    'K' => 13,
    'A' => 14,
  }
  def initialize(card)
    @value = card[0]
  end

  def value
    POINTS[@value]
  end
end
